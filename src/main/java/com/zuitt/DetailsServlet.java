package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class DetailsServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4484146589817098769L;

	/**
	 * 
	 */

	
	public void init() throws ServletException {
		System.out.println("******************************************");
		System.out.println(" DetailsServlet has been initialized. ");
		System.out.println("******************************************");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) 
			throws IOException {
		ServletContext srvContext = getServletContext();
		HttpSession session = req.getSession();
		
		String branding = srvContext.getInitParameter("branding");
		String fname = System.getProperty("fname");
		String lname = session.getAttribute("lname").toString();
		String email = srvContext.getAttribute("mail").toString();
		String contact = req.getParameter("contact");

		
		PrintWriter output = res.getWriter();
		output.println(
				"<h1>"+ branding +"</h1>"
				+ "<p>First Name: " + fname + "</p>"
				+ "<p>Last Name: " + lname + "</p>"
				+ "<p>Email: " + email + "</p>"
				+ "<p>Contact: " + contact + "</p>"
				);
	}
	
	public void destroy(){
		System.out.println("******************************************");
		System.out.println(" DetailsServlet has been destroyed. ");
		System.out.println("******************************************");
	}
}


